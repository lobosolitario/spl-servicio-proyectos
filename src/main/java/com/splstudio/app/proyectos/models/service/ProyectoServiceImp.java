package com.splstudio.app.proyectos.models.service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.splstudio.app.commons.models.entity.Elemento;
import com.splstudio.app.commons.models.entity.Proyecto;
import com.splstudio.app.proyectos.models.dao.ProyectoDAO;

@Service
public class ProyectoServiceImp implements IProyectoService{
	
	@Autowired
	private ProyectoDAO proyectoDAO;
	
	@Autowired 
	private RestTemplate clienteRest;

	@Override
	@Transactional(readOnly = true)
	public List<Proyecto> findAll() {
		return (List<Proyecto>) proyectoDAO.findAll();
	}
 
	@Override
	public Proyecto findById(Long id) {
		return proyectoDAO.findById(id).get();
	}


	@Override
	public Proyecto createOrUpdate(String jsonInfo) throws UnsupportedEncodingException {
		boolean res = true;

		try {
			String jsonDecoded = URLDecoder.decode(jsonInfo, StandardCharsets.UTF_8.toString());
			JSONObject proyectoJSON = new JSONObject(jsonDecoded);
			//Si el id es -1 se va a crear un nuevo proyecto
			Proyecto p = null;
			
			JSONObject proyecto =  proyectoJSON.getJSONObject("proyecto");
			String nombre = proyecto.getString("nombre");
			Long id = proyecto.getLong("id");
			
			if(id == -1) {
				//Si el id es -1 se crea un nuevo proyecto
				p = new Proyecto();
				p.setCreateAt(new Date());		
				p.setPermisos("0");
			}else {
				//Si el id es mayor que cero se obtiene el proyecto
				p = proyectoDAO.findById(id).get();
			}
			
			p.setModifiedAt(new Date());
			
			//Se actualiza el nombre del proyecto
			p.setNombre(nombre);
			proyectoDAO.save(p);
			
			
			
			//Actualizacion de elementos
			JSONObject elementos = new JSONObject();
			elementos.put("elementos", proyecto.getJSONArray("elementos"));
			elementos.put("idProyecto", p.getId());
			if(proyecto.has("eliminados")) {
				elementos.put("eliminados", proyecto.getJSONArray("eliminados"));
			}

			
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    
		    HttpEntity<String> request =  new HttpEntity<String>(elementos.toString(), headers);
			
			 res = res && (boolean) clienteRest.postForObject("http://localhost:8095/api/elementos/actualizar", request, Object.class);
			
			 return p;
		}catch(Exception e) {
			e.printStackTrace();
		}
		

		return null;
	}

	@Override
	public void eliminar(Long id) throws Exception {
		proyectoDAO.deleteById(id);
	}
	


}
