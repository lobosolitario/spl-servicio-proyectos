package com.splstudio.app.proyectos.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.splstudio.app.commons.models.entity.Proyecto;

public interface ProyectoDAO extends CrudRepository<Proyecto, Long>{
	
}
