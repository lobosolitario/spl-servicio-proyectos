package com.splstudio.app.proyectos.models.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.splstudio.app.commons.models.entity.Proyecto;


public interface IProyectoService {
	//Interface del servicio
	
	public List<Proyecto> findAll();
	public Proyecto findById(Long id)  throws Exception;
	public void eliminar(Long id)  throws Exception;
	public Proyecto createOrUpdate(String jsonInfo)  throws Exception;
}
