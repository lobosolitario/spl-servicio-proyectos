package com.splstudio.app.proyectos.controllers;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.splstudio.app.commons.models.entity.Elemento;
import com.splstudio.app.commons.models.entity.Proyecto;
import com.splstudio.app.proyectos.models.service.IProyectoService;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ProyectoController {
	
	@Autowired
	private IProyectoService proyectoService;
	

	
	@GetMapping("/listar")
	public List<Proyecto> listar() throws Exception {
		return proyectoService.findAll();
	}
	
	@GetMapping("/eliminar/{id}")
	public void eliminar(@PathVariable Long id)throws Exception  {
		proyectoService.eliminar(id);
	}
		
	
	@PostMapping("/actualizar")
	@ResponseStatus(HttpStatus.CREATED)
	public Proyecto update(@RequestBody  String infoProyecto) throws Exception {
		return proyectoService.createOrUpdate(infoProyecto);
	}

}
